import os

from eve import Eve
from eve.auth import BasicAuth
from eve_swagger import swagger


class MyBasicAuth(BasicAuth):

    def check_auth(self, username, password, allowed_roles, resource, method):
        return username == 'admin' and password == 'admin'


if 'PORT' in os.environ:
    port = int(os.environ.get('PORT'))
    host = '0.0.0.0'
else:
    port = 9090
    host = '0.0.0.0'

app = Eve(auth=MyBasicAuth)
app.register_blueprint(swagger)

app.config['SWAGGER_INFO'] = {
    'title': 'ccgp API',
    'version': '1.0',
    'description': '中国政府采购网站的集合汇总API',
    'contact': {
        'name': 'haohe',
        'email': 'hao.he@foxmail.com'
    },
    'license': {
        'name': 'BSD',
    },
    'schemes': ['http', 'https'],
}

if __name__ == '__main__':
    app.run(host=host, port=port)
