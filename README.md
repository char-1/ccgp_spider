# 求职Demo

因为时间的原因，做了一个简易性的爬虫，更多特性会慢慢完善，直到求职成功(●'◡'●)  

该Demo是爬取了各地的政府采购网（`ccgp`）并将信息进行汇总。通过提供的`http api`可以对爬取到的数据进行筛选查询。

## 特性
* 后台数据通过`http`协议上传， 采用了`mongo`存储（可以改用关系型数据库）
* 提供了`GET`方法进行数据的获取以及筛选
* 采用了`BasicAuth`对`POST`、`PATCH`、`DELETE`进行了身份验证
* 在`http server`层进行了数据去重
* 使用`celery`, 用`rabbitmq`做为消息队列，通过`http api`协议进行分布式上传
* 使用`celery beat`进行定时爬取

## 文件结构
`ccgp`是爬虫工程，采用了`scrapy`框架。`urls.csv`为待爬取的地址和获取相关信息的`xpath`表达式。想要爬取更多的信息，通过增加新的行即可。
 
`eve`是提供`http`服务工程，采用了基于`flask`的`eve`框架，实现了对数据的上传、获取等。


## 部署
docker
```
docker-compose build # 编译Dockerfile
docker-compose up -d # 运行服务编排，包括Mongo、rabbitmq、eve、ccgp
```

## 数据获取、筛选
访问`http://localhost:9090/ccgp`获取全部的数据。  
访问`http://localhost:9090/ccgp/<ccgp_id>`获取单条数据。  
访问`http://localhost:9090/ccgp?where={"site": "河南省政府采购网"}`获取指定条件的数据。 


## TODO
- [ ]  采用`elasticsearch`实现实时的分布式的搜索分析
- [ ]  用`redis`进行简易的去重