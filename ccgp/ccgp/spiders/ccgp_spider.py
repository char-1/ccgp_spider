# -*- coding: utf-8 -*-
import scrapy
import csv
from os import path
from datetime import datetime
from urllib.parse import urlparse
from ccgp.items import CcgpItem


class CcgpSpider(scrapy.Spider):
    name = "ccgp"
    # allowed_domains = ["ccgp.com"]
    start_urls = []

    def __init__(self, crawl_date):
        self.crawl_date = crawl_date
        self.urls = {}
        self.load_csv()

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            crawl_date=crawler.settings.get('CRAWL_DATE')
        )

    def parse(self, response):
        host_name = urlparse(response.url).hostname
        xpath_info = self.urls[host_name]

        end_flag = False
        for sel in response.xpath(xpath_info['xpath']):
            item = CcgpItem()
            item['site'] = xpath_info['site_title']
            item['title'] = self.get_title(sel, xpath_info)
            item['url'] = self.get_url(sel, response, xpath_info)
            item['date'] = self.get_date(sel, xpath_info)

            item_datetime = datetime.strptime(item['date'], '%Y-%m-%d')
            crawl_datetime = datetime.strptime(self.crawl_date, '%Y-%m-%d')
            if item_datetime < crawl_datetime:
                end_flag = True
                return
            yield item

        if not end_flag:
            next_page = self.get_next_page(response, xpath_info)
            if next_page:
                yield scrapy.Request(next_page, callback=self.parse)

    def load_csv(self):
        with open('urls.csv', 'r', encoding='gbk') as f:
            reader = csv.reader(f)
            first_row = True
            for line in reader:
                if first_row:
                    first_row = False
                    continue
                site_title = line[0]
                url = line[1]
                xpath = line[2]
                xpath_title = line[3]
                xpath_url = line[4]
                xpath_date = line[5]
                date_format = line[6]
                next_page_title = line[7]
                self.urls[urlparse(url).hostname] = {
                    "site_title": site_title,
                    "xpath": xpath,
                    "title": xpath_title,
                    "url": xpath_url,
                    "date": xpath_date,
                    "date_format": date_format,
                    "next_page_title": next_page_title
                }
                CcgpSpider.start_urls.append(url)

    def get_title(self, sel, xpath_info):
        for title in sel.xpath(xpath_info["title"]).extract():
            title = title.strip()
            if title:
                return title

    def get_url(self, sel, response, xpath_info):
        for url in sel.xpath(xpath_info["url"]).extract():
            url = url.strip()
            if url:
                return response.urljoin(url)

    def get_date(self, sel, xpath_info):
        date_format = xpath_info["date_format"]
        for date in sel.xpath(xpath_info["date"]).extract():
            date = date.strip()
            if date:
                return datetime.strptime(date, date_format).strftime('%Y-%m-%d')

    def get_next_page(self, response, xpath_info):
        next_page_title = xpath_info["next_page_title"]
        for sel in response.xpath("//a"):
            for title in sel.xpath('text()').extract():
                title = title.strip()
                if -1 != title.find(next_page_title):
                    for next_page in sel.xpath('@href').extract():
                        next_page = next_page.strip()
                        if next_page:
                            return response.urljoin(next_page)
